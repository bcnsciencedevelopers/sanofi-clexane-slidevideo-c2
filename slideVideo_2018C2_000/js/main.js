/*
 * Init fastclick
 */
/*if ('addEventListener' in document) {
 document.addEventListener('DOMContentLoaded', function () {
 FastClick.attach(document.body);
 }, false);
 }*/




$(document).ready(function () {

    $('[data-slide]').on('click', function () {
        var auxSlide = $(this).data('slide');

        if (window.navigator.standalone === undefined) {
            location.href = "../" + auxSlide + "/index.html";
        } else {
            window.parent.navigateToSequence(auxSlide);
        }
    });

    if (window.navigator.standalone === undefined) {
        localDevelopment();
    }

    $('[data-popup]').on('click', function () {
        $('.popup').removeClass('active');
        var pop = $(this).data('popup');
        var id = $(this).attr("id");
        if (pop != '') {
            $('#' + pop).addClass('active');
//            console.log(id);
            if (id == 'boton_video1') {
//                $('#info1').css('display', 'block');
                document.getElementById('vid1').play();
                $('#vid1').addClass('active');
            } else {
//                $('#info2').css('display', 'block');
                document.getElementById('vid2').play();
                $('#vid2').addClass('active');
            }
            //data-prevent-left-swipe="false" data-prevent-right-swipe="false"
            $("body").attr("data-prevent-left-swipe", true);
            $("body").attr("data-prevent-right-swipe", true);
            if (pop == "popup_analisis_mercado" || pop == "popup_analisis_mercado2" || pop == "popup_info" ||
                    pop == "popup_graficas" || pop == "popup_esquema") { //segundo nivel de popups...
                $('#lizipaina').addClass('active');
            }
        }

        if (id == "close_popup_esquema" || id == "close_popup_graficas" || id == "close_popup_info") {
            $('#lizipaina').addClass('active');
        }
    });

    $(".popup_close").on("click", function () {
        $("body").attr("data-prevent-left-swipe", false);
        $("body").attr("data-prevent-right-swipe", false);
    });

    $('[data-pdf]').on('click', function () {
        var pdf = jQuery(this).data('pdf');

        console.log('open pdf: ' + pdf);
        if (window.parent.PDFHelper.OpenPDF) {
            window.parent.PDFHelper.OpenPDF('media/pdf/' + pdf + '.pdf', window, true);
        } else {
            window.open('media/pdf/' + pdf + '.pdf');
        }
    });

    /* problemas con swipers */
    $("#btn_popup_analisis_rojo").on("click", function () {

        $("#da_problemas").css("z-index", "-1");
    });

    /* lizivoz swiper */
    $("#btn_lizivoz").on("click", function () {
        swiper[0].slideNext();
    });

//    $('#play1').on('click', function () {
//        document.getElementById('vid1').play();
//        $('#vid1').addClass('active');
//    });
//    $('#play2').on('click', function () {
//        document.getElementById('vid2').play();
//        $('#vid2').addClass('active');
//    });

    $('.popup_close').on('click', function () {
        
        document.getElementById('vid1').pause();
        document.getElementById('vid1').load();
        $('#vid1').removeClass('active');
        document.getElementById('vid2').pause();
        document.getElementById('vid2').load();
        $('#vid2').removeClass('active');
        $('#info1').css('display', 'none');
        $('#info2').css('display', 'none');
    });

});

function localDevelopment() {
    console.info('Local development is ready');

    var next = "";
    var prev = "";

    var jqxhr = $.get("parameters/parameters.xml", function (data) {
    })
            .done(function (data) {
                console.info('read parameters.xml OK');

                var sequenceId = '';

                var found = 0;

                $(data).find('Sequence').each(function () {
                    console.info('sequenceId : ' + $(this).attr('Id'));
                    sequenceId = $(this).attr('Id');
                });

                var cont = -1;
                var slides = Array();
                $(data).find('Link').each(function () {
                    cont++;
                    if (sequenceId == $(this).attr('SequenceId')) {
                        found = cont;
                    }
                    slides.push($(this).attr('SequenceId'))
                });

                //si ha encontrado el slide
                if (found >= 0) {
                    if ((slides.length - 1) == found) { //limite max array
                        console.log('limite max');
                        prev = slides[found - 1];
                    } else if (found == 0) { //limite min array
                        console.log('limite min');
                        next = slides[found + 1];
                    } else {
                        prev = slides[found - 1];
                        next = slides[found + 1];
                    }
                } else {
                    console.error('No ha encontrado el slide');
                }

                console.log('prev : ' + prev);
                console.log('next : ' + next);

            })
            .fail(function (data) {
                alert("Error: reading xml failed");
            })
            .always(function () { });



    var page = document.body;
    var mc = new Hammer(page);
    mc.get('swipe').set({direction: Hammer.DIRECTION_ALL});
    mc.on("swipeup swipedown swipeleft swiperight", function (ev) {
        if (ev.type == "swipeleft") { //next
            if (next != '') {
                location.href = "../" + next + "/index.html";
            }
        } else if (ev.type == "swiperight") {  //prev
            if (prev != '') {
                location.href = "../" + prev + "/index.html";
            }
        }
    });
}